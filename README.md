This repository showcases a workaround for the limitation that [dotenv variables](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv) can't be used in `rules:`. The reason for this limitation is that `rules:` are parsed at pipeline creation time, but dotenv variables are only created during runtime of a pipeline.

By explicitly passing the dotenv variables to a downstream pipeline, they can be used with `rules:` in jobs in that downstream pipeline.

---

The `.gitlab-ci.yml` file in this project has a job `set-variables` that defines a dotenv variable. The two jobs `testjob` and `testjob_verification` in that same file are used as a baseline: Only the latter will run, because the former has `rules:` that require the dotenv variable to be set. While the variable can be accessed in the `script:` section of jobs in the same pipeline (as that is happening at runtime), the `testjob` doesn't get added to the pipeline in the first place.

The `start-child` job the passes the `RUN_TESTJOB` variable via the `trigger:` keyword to start a child pipeline. The `.child.yml` file for said child pipeline defines same two jobs `testjob` and `testjob_verification` as present in the parent pipeline definition. In this context, both jobs get added because `RUN_TESTJOB` is now available at pipeline creation time for the child pipeline.